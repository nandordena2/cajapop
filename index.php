<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CraftPop</title>
        <link rel="icon" type="image/png" href="/img/icon.png">

        <link rel="stylesheet" href="/frontend/vista/formulario.css">
        <script type="text/javascript" src="/frontend/controlador/usuarios.js"> </script>
        <script type="text/javascript" src="/frontend/vista/formulario.js"> </script>
        <script type="text/javascript">
            window.addEventListener("load",()=>{
                registro = new FormularioRegistro();
                document.body.appendChild(registro.main);
            });
        </script>
    </head>
    <body> </body>
</html>
