/*
    path: /frontend/controlador/usuarios.js
*/
if(typeof accesoDatos == "undefined"){
    accesoDatos = {};
}
accesoDatos.singin = function(email,nikname,password){
    var formData = new FormData();
    formData.append("email",email);
    formData.append("nikname",nikname);
    formData.append("password",password);
    
    return fetch(
        window.location.protocol+"//"+(window.location.origin.match(/local/g)==null?'craftpop.com':'craftpop.local')
        +"/backend/usuarios/usuario_insert.php"
        ,{
            method:'POST'
            ,body : formData
        }
    ).then(json=>json.json());
}
