/*
    path: /frontend/vista/formulario.js
*/
class FormularioRegistro {
    constructor() {
        this.main = document.createElement('div');
        this.main.className='FormularioRegistro';

        this.email = document.createElement('input');
        this.email.type = "text";
        this.email.placeholder ="email";
        this.email.className = "email";
        this.main.appendChild(this.email);

        this.nikname = document.createElement('input');
        this.nikname.type = "text";
        this.nikname.placeholder = "nikname";
        this.nikname.className = "nikname";
        this.main.appendChild(this.nikname);

        this.password = document.createElement('input');
        this.password.type = "password";
        this.password.placeholder = "password";
        this.password.className = "password";
        this.main.appendChild(this.password);

        this.repassword = document.createElement('input');
        this.repassword.type = "password";
        this.repassword.placeholder = "repassword";
        this.repassword.className = "repassword";
        this.main.appendChild(this.repassword);

        this.button = document.createElement('button');
        this.button.innerHTML = "Sign In";
        this.button.className = "repassword";
        this.button.addEventListener("click",()=>{
            this.singIn(
                this.email.value
                ,this.nikname.value
                ,this.password.value
                ,this.repassword.value
            );
        });
        this.main.appendChild(this.button);
    }
    singIn(email,nikname,password,repassword){
        if(
            email.search(/[a-z0-9\.\_]{3,50}\@[a-z0-9\.\-]{4,250}/) != -1
            && this.nikname.value.search(/[a-z0-9\.\_]{3,50}/) !=-1
            && this.password.value == this.repassword.value
        ){
            accesoDatos.singin(email,nikname,password)
            .then(datos=>{
                if(datos.error == null){
                    console.info("REGISTRADO");
                }else{
                    console.info("FALLO REGISTRO");
                    this.main.setAttribute("error","1");
                }
            });
        }
        else{
            this.main.setAttribute("error","1");
            console.info("FALLO REQUISITOS");
        }
    }
}
